Title: sobre

O hackerspace-feec atualmente é um grupo de estudantes organizados/as a fim de incentivar o desenvolvimento de projetos criativos pelos/as estudantes e facilitar a divulgação de conhecimentos entre os/as estudantes. Isto se dará tanto através da criação de canais de comunicação e uma comunidade, quanto através do fomento à organização de eventos pelos/as próprios/as estudantes acerca de temas que queiram compartilhar e/ou ensinar.

O maior objetivo do hackerspace atualmente é conseguir um espaço físico com o apoio dos/as docentes e funcionários/as da faculdade. Também é objetivo do hackerspace a criação de um canal de comunicação entre os/as estudantes em geral para aumentar o alcance da comunidade e definir os próximos passos a serem seguidos.

A proposta inicial de criação do hackerspace com mais informações segue abaixo:

[Proposta de criação do hackerspace](https://hackerspacefeec.gitlab.io/proposta-de-criacao-de-um-hackerspace-na-feec.html)

__Contatos:__

Nícolas ( nfraprado (at) protonmail (dot) com )

Lucas ( lzenichi (at) protonmail (dot) com )

Rafael ( rafaelgoncalves (at) riseup (dot) net )



