Title: Nome: hackerspace vs makerspace
Date: 2019-08-06

---

(texto baseado em conversa sobre o porquê do uso do nome hackerspace em oposição ao nome makerspace)

O termo hackerspace nos é bastante caro desde o início por conta de estar intimamente relacionado com uma cultura de compartilhamento livre de conhecimento, práticas que visam explorar as tecnologias de formas convencionais ou não e tudo que historicamente está ligado ao movimento hacker.
Também vemos como algo bastante bom contribuirmos para o debate acerca do termo 'hacker' no sentido de ressignificar o mesmo que atualmente é usado de forma pejorativa pelas mídias em geral.

É de nosso interesse que, uma vez que nos estabeleçamos num espaço físico, haja um gerenciamento do espaço pelos/as próprios/as usuários/as, no nosso caso, em especial os/as estudantes.
Entendemos que o gerenciamento do espaço pelos/as próprios/as estudantes é muito importante para que os/as mesmos/as se sintam responsáveis por ele e cuidem do espaço ao invés de esperar isso apenas das pessoas que o gerenciam propriamente dito, evitando que seja replicado o problema que já existe em outro laboratório da FEEC.
Esse tipo de cultura e gestão é bastante comum em hackerspaces.

Quanto à liberdade para atividades, desde que os/as estudantes estejam organizando atividades em busca de conhecimento tanto teórico quanto prático, e com intercâmbio de ideias entre eles, não parece ser um problema a irrestrição ao assunto, principalmente porque a grande maioria dos assuntos abordados serão inevitavelmente voltados para elétrica e computação, dada a natureza da faculdade. Acreditamos que o coletivismo também é bastante positivo para o desenvolvimento dos membros, pois como pessoas vivemos no mundo coletivamente. Trabalhamos em grupos, estudamos em grupos e fomentar a colaboração e interação entre os/as membros do espaço é algo que gostaríamos de ver acontecer no ambiente. Essas são características comuns a hackerspaces.

Atividades de prototipagem em eletrônica certamente serão um dos principais usos do espaço, mas não gostaríamos que fosse o único. Outro ponto importante (explicitado no documento atual do hackerspace) é o intercâmbio de experiências e conhecimentos entre estudantes. Com esse fim que surgiu a ideia de haver também no espaço, além de equipamentos de prototipagem, uma lousa/televisão com assentos em torno. O termo hackerspace tem a ideia de compartilhamento do conhecimento enraízada (o que parece muito pertinente ao clima de universidade), o que não pode ser dito para o termo makerspace, que está mais voltado para a disponibilização de um espaço com ferramental por si só.

Além disso, historicamente, hackerspaces são muito mais voltados aos temas de computação e eletrônica (principal interesse de estudantes da FEEC), do que makerspaces, que comumente contam com equipamentos para trabalho com madeira, por exemplo.

Alguns exemplos de hackerspaces no Brasil:

 - [LHC - Campinas](https://lhc.net.br/wiki/P%C3%A1gina_principal)
 - [Garoa - São Paulo](https://garoa.net.br/wiki/P%C3%A1gina_principal)

Exemplos de hackerspaces de universidades estadunidenses:

 - <https://cclub.cs.wmich.edu/>
 - <https://umaine.edu/hackerspace/>
