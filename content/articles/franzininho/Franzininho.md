title: Franzininho
tags: attiny85, franzininho, eletronica
date: 02-17-2020

---

O Franzininho é uma placa de desenvolvimento - semelhante a um Arduino - baseada no microcontrolador ATtiny85.
O projeto é brasileiro e possui hardware aberto publicado com a licensa Creative Commons BY-SA.
O design da PCB foi feito em uma única camada para facilitar a montagem em casa e, após gravar um bootloader, a placa pode ser programada utilizando a interface arduino. Por conta de sua facilidade, o projeto se mostra uma ótima porta de entrada para a programação de embarcados.

<br/>
<strong>Especificações:</strong>

 - 6 GPIOs
 - 4 pinos ADC
 - 3 pinos PWM
 - Comunicação: I2C, SPI
 - Tensão de funcionamento: 5V
 - Alimentação externa: 7,5-9V
 - Bootloader: [Micronucleus](https://github.com/micronucleus/micronucleus)


<br/>
<img style='display: block; align: center; margin-right: auto; margin-left:auto; max-width: 90%; height: auto;' alt='Attiny pinout' src='../../images/franzininho.jpeg'/>

<br/>
<strong>Pinagem:</strong>
<br/>
<img style='display: block; align: center; margin-right: auto; margin-left:auto; max-width: 90%; height: auto;' alt='Attiny pinout' src='../../images/franzininho_pinout.png'/>

<br/>
<strong>Links do projeto:</strong>

[Site do projeto](https://franzininho.com.br/projeto/fanzininho-diy)

[Documentação](https://franzininho.gitbook.io/franzininho-docs/)

[Repositório Git](https://github.com/Franzininho/franzininho-diy-board)
