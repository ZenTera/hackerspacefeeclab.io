#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

from pelican_jupyter import markup as nb_markup

AUTHOR = 'hackerspacefeec'
SITENAME = 'hackerspace FEEC'
DESCRIPTION = ''
# SITEURL = 'hackerspacefeec.gitlab.io'

THEME = 'theme'
# PYGMENTS_STYLE = 'monokai'
SITEIMAGE = '/theme/images/glider.jpg width=135 height=135'
RFG_FAVICONS = True

ICONS = (
    ('gitlab', 'https://gitlab.com/hackerspacefeec'),
    ('comments', 'https://matrix.to/#/!fefUMvuSJqVsEDWhvS:matrix.org?via=matrix.org')
)

PATH = 'content'

OUTPUT_PATH = 'public/'

STATIC_PATHS = [
        'images',
        'extra',
        ]

PLUGINS = [nb_markup]
MARKUP = ('md', 'ipynb')
IGNORE_FILES = ['.ipynb_checkpoints']
# IPYNB_SKIP_CSS = False
# IPYNB_EXPORT_TEMPLATE = 'theme/static/css/jupyter.tpl'

EXTRA_PATH_METADATA = {
     'extra/favicon.ico': {'path': 'favicon.ico'},
}

TIMEZONE = 'America/Sao_Paulo'

DEFAULT_LANG = 'pt_br'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
